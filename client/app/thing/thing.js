'use strict';

angular.module('cmprojectApp')
  .config(function ($stateProvider) {
    $stateProvider.state('things', {
        url: '/things',
        template: '<thing></thing>'
      }).state('/things/:name', {
        url: '/things/:name',
        template: '<id></id>'
      });
  });
