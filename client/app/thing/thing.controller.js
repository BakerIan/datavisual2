'use strict';
(function(){

class ThingController {
  constructor($http, $scope, socket) {
    this.$http = $http;
    this.socket = socket;
    this.awesomeThings = [];
    this.scope = $scope;
  }

  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
        this.socket.syncUpdates('thing', this.awesomeThings);
      });
  }

  addThing() {
    if (this.newThing && this.newTime && this.newKeywords && this.newInterval) {
      this.$http.post('/api/things', {
        intervalTime: this.newInterval,
        name: this.newThing,
        keywords: this.newKeywords.split(', '),
        duration: this.newTime,
        created_at: new Date(Date.now()),
        end_at: new Date(Date.now() + this.newTime *3600*1000),
        stats: {}
      });
      this.newThing = '';
      this.newTime = '';
      this.newKeywords = '';
      this.newInterval = '';
    }
  }


  deleteThing(thing) {
    console.log('deleting thing');
    this.$http.delete('/api/things/' + thing._id);
  }

  focus(thing){
    //console.log('showing thing');
    this.$http.get('/api/things/' + thing._id)
    .then(response => {
        this.curThing =response.data;
        this.socket.syncUpdates('curThing', this.curThing)
        console.log(JSON.stringify(this.curThing));
        $scope.curThing = response.data;
    });
  }
}



angular.module('cmprojectApp')
  .component('thing', {
    templateUrl: 'app/thing/thing.html',
    controller: ThingController,
  });
angular.module('cmprojectApp')
    .component('id', {
      templateUrl: 'app/thing/_id.html',
      controller: ThingController,
    });

})();
