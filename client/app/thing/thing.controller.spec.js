'use strict';

describe('Controller: ThingController', function () {

  // load the controller's module
  beforeEach(module('cmprojectApp'));

  var ThingController, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    ThingController = $componentController('ThingController', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
