(function(angular, undefined) {
'use strict';

angular.module('cmprojectApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);