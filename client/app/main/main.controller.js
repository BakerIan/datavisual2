'use strict';

(function() {

  class MainController {

    constructor($http, $scope, Upload, socket) {
      this.$http = $http;
      this.socket = socket;
      this.awesomeThings = [];
      $scope.$on('$destroy', function() {
        socket.unsyncUpdates('thing');
      });

      setInterval(function(){
        $scope.$apply();
      }, 1000);
    }


    $onInit() {
      this.$http.get('/api/things')
        .then(response => {
          this.awesomeThings = response.data;
          this.socket.syncUpdates('thing', this.awesomeThings);
        });



    }


////Purpose
///Expected input
///expected output
    addThing() {
      if (this.newThing && this.newTime && this.newKeywords) {

      this.$http.post('/api/things', {
          name: this.newThing,
          intervalTime: this.newInterval,
          keywords: this.newKeywords.split(', '),
          duration: 60000,//(new Date(this.newTime))-(new Date(Date.now())),
          created_at: new Date(Date.now()),
          end_at: this.newTime,
          status: "Active",
          IntervalType: this.IntervalType,
          levels: this.newLevels,
          statLength: 0
        });
        this.newThing = '';
        this.newTime = '';
        this.newKeywords = '';
        this.newInterval = '';
        this.IntervalType='';
        this.newLevels='';
      }
    }

    addThingHistorical(){

      if (this.newThingH && this.startDate  && this.endDate && this.newIntervalH && this.newKeywordsH){
        this.$http.post('/api/things', {
          name: this.newThingH,
          intervalTime: this.newIntervalH,
          keywords: this.newKeywordsH.split(', '),
          duration: (this.endDate-this.startDate),
          created_at: this.startDate,
          end_at: this.endDate,
          IntervalType: this.IntervalTypeH,
          status: "Historical",
          levels: this.newLevelsH,
          statLength: 0
        });

      }
      this.newThingH= '';
      this.newIntervalH='';
      this.startDate='';
      this.endDate='';
      this.newKeywordsH='';
      this.IntervalTypeH='';
      this.newLevelsH='';
    }


    addFile($scope){
      this.$http.post('/api/things/add', {
        fileType: this.fileType,
        fileName: this.fileName.name,
        fileQueryName: this.fileQueryName
      });
    }


    deleteThing(thing) {
      this.$http.delete('/api/things/' + thing._id);
    }

    /*sendJson(){
        this.$http.get('/api/things')
          .then(response => {
            this.awesomeThings = response.data;
          });

          var blob = new Blob([this.awesomeThings ], { type: 'text/json'});
          $scope.url = (window.URL || window.webkitURL).createObjectURL( blob );
      console.log(encodeURIComponent(JSON.stringify(this.awesomeThings)));
      return encodeURIComponent(JSON.stringify(this.awesomeThings));
    }*/

    focus(thing){
      //console.log('showing thing');
      this.$http.get('/api/things/' + thing._id)
      .then(response => {
          this.curThing =response.data;
          this.socket.syncUpdates('curThing', this.curThing);
      });
    }




  }
  angular.module('cmprojectApp')
    .component('main', {
      templateUrl: 'app/main/main.html',
      controller: MainController
    });
    angular.module('cmprojectApp')
    .directive('fileReader', function() {
  return {
    scope: {
      fileReader:"="
    },
    link: function(scope, element) {
      $(element).on('change', function(changeEvent) {
        var files = changeEvent.target.files;
        //console.log(files.length);

        if (files.length) {
          var r = new FileReader();
          r.onload = function(e) {
              var contents = e.target.result;
              scope.$apply(function () {
                scope.fileReader = contents;
              });
              //console.log(scope.fileReader + ' in onload');
              //r.readAsText(files[0]);
          };
          console.log('using file-reader directive');
          console.log(r);
          //r.readAsText(files[0]);
        }
      });
    }
  };
});
  angular.module('cmprojectApp')
    .directive('linearChart',  function($parse, $window){
      return {
        restrict: 'EA',
        template:"<svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg><svg width='300' height='300'></svg>",
        link: function(scope, elem, attrs){

          var keys = ['W0', 'W1', 'W2', 'W3','W4', 'W5', 'W6', 'W7', 'W8', 'W9'];
          var keysL2 = ['W0', 'W1', 'W2', 'W3', 'W4'];
          var colors = ['aqua', 'blue', 'chartreuse', 'hotpink', 'saddlebrown', 'green', 'darkred', 'darkorange', 'grey', 'black'];
          var dataToPlot=scope.$eval(attrs.chartData);
          dataToPlot = dataToPlot.data;
          var level = scope.stat.level;
          //console.log(scope.stat.level);
        //  console.log(JSON.stringify(dataToPlot));
          var scaleData = [];
          var loopdata = [];
          var iLoop, jLoop;

          if (level == 1 ){
            loopdata = keys;
            iLoop = 8;      //# of features
            jLoop = 8;
          }
          else if (level == 2 ) {
            loopdata = keys;
            iLoop = 5;    //# of features
            jLoop = 5;
          }
          for(var k in loopdata){
              for (var i = 0; i<iLoop; i ++){
                for (var j = 0; j<jLoop; j++){
                    dataToPlot[loopdata[k]][i][j] = parseFloat(dataToPlot[loopdata[k]][i][j]);
                    scaleData.push(dataToPlot[loopdata[k]][i][j]);
                }
              }
          }

          var padding = 20;
          var pathClass = "path";
          var xScale, yScale, xAxisGen, yAxisGen, lineFun, svg;
          var d3 = $window.d3;
          var rawSvg = elem.find('svg');



          function setChartParameters(index, wIndex, level){
            svg = d3.select(rawSvg[wIndex]);
              //console.log(level, wIndex, index);
            if (level ==1 ){
              xScale = d3.scale.linear()
               .domain(d3.extent(dataToPlot[keys[wIndex]][index], function(d,i) {
                   return i;
               }))
               .range([padding + 5, 200 - padding]);

               xAxisGen = d3.svg.axis()
                  .scale(xScale)
                  .orient("bottom")
                  .ticks(7);
            }
            else if (level == 2) {
              //console.log(level, wIndex, index);
              xScale = d3.scale.linear()
               .domain(d3.extent(dataToPlot[keys[wIndex]][index], function(d,i) {
                   return i;
               }))
               .range([padding + 5, 200 - padding]);

               xAxisGen = d3.svg.axis()
                  .scale(xScale)
                  .orient("bottom")
                  .ticks(5)
                  .tickFormat(function (d, i){
                      return i*3;
                  });
            }

            yScale = d3.scale.linear()
                .domain(d3.extent(scaleData, function(d) {
                  //console.log(d);
                    return d;
                }))
             .range([200,0]);



            yAxisGen = d3.svg.axis()
               .scale(yScale)
               .orient("left")
               .ticks(5);

            lineFun = d3.svg.line()
              .x(function (d, i) {
                return xScale(i);
              })
              .y(function (d, i) {
                return yScale(d);
              })
              .interpolate("basis");



            }

            function drawLineChart(index, wIndex, level) {

              setChartParameters(index, wIndex, level);

              svg.append("svg:g")
                 .attr("class", "x axis")
                  .attr("transform", "translate(20,220)")
                  .call(xAxisGen);

              if ( level == 1){
                svg.append("svg:g")
                   .attr("class", "y axis")
                   .attr("transform", "translate(40,20)")
                   .call(yAxisGen)
                   .append("text")
                     .attr("y", 6)
                     .attr("dy", ".71em")
                     .attr("transform", "translate(100,240)")
                     .style("text-anchor", "end")
                     .text(keys[wIndex]);

                svg.append("svg:path")
                   .attr("transform", "translate(20,20)")
                   .attr({
                     d: lineFun(dataToPlot[keys[wIndex]][index]),
                   "stroke": function() {
                         return colors[index]; },
                     "stroke-width": 2,
                     "fill": "none",
                     "class": pathClass
                });


                var color = d3.scale.ordinal()      //For legend of graphs
                    .domain(["1st-init", "1st-prop", "1st-comt", "follow-prop", "follow-comt", "rec-init", "rec-prop", "rec-comt"])
                    .range(colors);


                var legend = svg
                    .append("g")
                    .selectAll("g")
                    .data(color.domain())
                    .enter()
                    .append('g')
                      .attr('class', 'legend')
                      .attr('transform', function(d, i) {
                        var height = 10;
                        var x = 210;
                        var y = 10+ i * height;
                        return 'translate(' + x + ',' + y + ')';
                      });
                legend.append('rect')
                    .attr('width', 10)
                    .attr('height', 10)
                    .style('fill', color)
                    .style('stroke', color);

                legend.append('text')
                    .attr('x', 10 + 1)
                    .attr('y', 10 - 1)
                    .text(function(d) { return d; });

              }
              else if (level == 2) {
                svg.append("svg:g")
                   .attr("class", "y axis")
                   .attr("transform", "translate(40,20)")
                   .call(yAxisGen)
                   .append("text")
                     .attr("y", 6)
                     .attr("dy", ".71em")
                     .attr("transform", "translate(100,240)")
                     .style("text-anchor", "end")
                     .text(keys[wIndex]);

                svg.append("svg:path")
                   .attr("transform", "translate(20,20)")
                   .attr({
                     d: lineFun(dataToPlot[keys[wIndex]][index]),
                   "stroke": function() {
                         return colors[index]; },
                     "stroke-width": 2,
                     "fill": "none",
                     "class": pathClass
                });

                var color = d3.scale.ordinal()
                    .domain(["W0", "W1", "W2", "W3", "W4"])
                    .range(colors);


                var legend = svg
                    .append("g")
                    .selectAll("g")
                    .data(color.domain())
                    .enter()
                    .append('g')
                      .attr('class', 'legend')
                      .attr('transform', function(d, i) {
                        var height = 10;
                        var x = 220;
                        var y = 10+ i * height;
                        return 'translate(' + x + ',' + y + ')';
                      });
                legend.append('rect')
                    .attr('width', 10)
                    .attr('height', 10)
                    .style('fill', color)
                    .style('stroke', color);

                legend.append('text')
                    .attr('x', 10 + 1)
                    .attr('y', 10 - 1)
                    .text(function(d) { return d; });

              }
            }



            if (level ==1 ){
                for (var k = 0; k<10; k++) {
                  for (var j = 0; j<8; j++){
                    drawLineChart(j, k, level);
                  }
                }
            }
            else if (level ==2) {
              for (var k = 0; k<10; k++) {
                for (var j = 0; j<5; j++){
                  drawLineChart(j, k, level);
                }
              }
            }
        }
      }
    })


})();
