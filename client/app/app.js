'use strict';

angular.module('cmprojectApp', [, 'cmprojectApp.auth', 'cmprojectApp.admin', 'cmprojectApp.constants',
    'ngCookies', 'ngResource', 'ngSanitize', 'btford.socket-io', 'ui.router', 'ui.bootstrap',
    'validation.match', 'ngFileUpload'
  ])
  .config(function($urlRouterProvider, $locationProvider, $compileProvider ) {

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|tel|file|blob):/);
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);
  });
