'use strict';

angular.module('cmprojectApp.auth', ['cmprojectApp.constants', 'cmprojectApp.util', 'ngCookies',
    'ui.router'
  ])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
