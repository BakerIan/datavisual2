from __future__ import division
__author__ = 'tianxi'

import json,math,time,numpy,sys
from datetime import date, datetime, timedelta





if len(sys.argv) > 1:
    reading_file_name = sys.argv[1]
    file_name = sys.argv[2]
with open(reading_file_name) as json_data:
    data = json.load(json_data)




# with open('fff1468563749354input.json') as json_data:
#     data = json.load(json_data)
#
# file_name = 'hopework.json'


keywords = data['keywords']

tweets = data['tweets']



""" generate time steps """
first_created_at = tweets[0]['created_at'][:-5]
last_created_at = tweets[-1]['created_at'][:-5]
# print(first_created_at)
# print(last_created_at)
# fmt = '%Y-%m-%dT%H:%M:%S'
# d1 = datetime.strptime(first_created_at, fmt)
# d2 = datetime.strptime(last_created_at, fmt)
# # convert to unix timestamp
# d1_ts = time.mktime(d1.timetuple())
# d2_ts = time.mktime(d2.timetuple())
# timestep_length = math.ceil(int(d2_ts-d1_ts)/60)
def datetime_range(start, end, delta):
    current = start
    if not isinstance(delta, timedelta):
        delta = timedelta(**delta)
    while current <= end:
        yield current
        current += delta
start = datetime(int(first_created_at[:4]),int(first_created_at[5:7]),int(first_created_at[8:10]),
                                                                          int(first_created_at[11:13]),int(first_created_at[14:16]))
end = datetime(int(last_created_at[:4]),int(last_created_at[5:7]),int(last_created_at[8:10]),
                                                                      int(last_created_at[11:13]),int(last_created_at[14:16]))
time_step_list = []
for dt in datetime_range(start, end, {'minutes':1}):
    dt = str(dt).replace(' ','T')[:-3]
    time_step_list.append(dt)
# print(time_step_list)


tweet_keyword_list = []
for tweet in tweets:
    try:
        tweet_keyword_list.append(tweet['keywords'][0])
    except:
        tweet_keyword_list.append('')

DYNAMIC_MATRIX = []

for a_keyword in keywords:
    keyword_indices = [i for i, x in enumerate(tweet_keyword_list) if x == a_keyword]
    # c = [tweet_keyword_list[index] for index in keyword_indices]
    # print(c)
    filtered_tweets = [tweets[index] for index in keyword_indices]
    pool_BeRetweeted_tweet_id = []
    pool_BeReplied_tweet_id = []
    pool_initiator = []
    pool_user = []
    tweet_time = []
    for tweet in filtered_tweets:
        created_at = tweet['created_at'][:16]
        tweet_time.append(created_at)
    # print(tweet_time)
    dynamic_matrix = []
    for a_time in time_step_list:

        first_initiator = 0
        first_prop = 0
        first_comt = 0
        follow_prop = 0
        follow_comt = 0
        re_initiator = 0
        re_prop = 0
        re_comt = 0
        index = numpy.where(numpy.array(tweet_time) == a_time)[0]
        if len(index) == 0:
            dynamic_matrix.append([first_initiator,first_prop,first_comt,
                                   follow_prop,first_comt,re_initiator,re_prop,re_comt])
            # print(a_time)
            continue
        sub_tweets = list(map(tweets.__getitem__, index))
        for tweet in sub_tweets:
            if tweet['type'] == "original":
                if tweet['user_id'] in pool_initiator:
                    re_initiator = re_initiator + 1
                else:
                    first_initiator = first_initiator + 1
                    pool_initiator.append(tweet['user_id'])
            elif tweet['type'] == 'reply':
                if tweet['user_id'] in pool_user:
                    re_comt = re_comt+1
                else:
                    pass
                if tweet['in_reply_to_tweet_id'] in pool_BeReplied_tweet_id:
                    follow_comt = follow_comt+1
                else:
                    first_comt = first_comt+1
                    pool_BeReplied_tweet_id.append(tweet['in_reply_to_tweet_id'])
            elif tweet['type'] == 'retweet':
                if tweet['user_id'] in pool_user:
                    re_prop = re_prop+1
                else:
                    pass
                if tweet['in_reply_to_tweet_id'] in pool_BeRetweeted_tweet_id:
                    follow_prop = follow_prop+1
                else:
                    first_prop = first_prop+1
                    pool_BeRetweeted_tweet_id.append(tweet['in_reply_to_tweet_id'])
            else:
                pass
            pool_user.append(tweet['user_id'])
        dynamic_matrix.append([first_initiator,first_prop,first_comt,
                                   follow_prop,first_comt,re_initiator,re_prop,re_comt])
    num_rows = len(dynamic_matrix)
    num_col = len(dynamic_matrix[0])
    transposed_dynamic_matrix = []
    for i in range(num_col):
        transposed_dynamic_matrix.append([row[i] for row in dynamic_matrix])
    DYNAMIC_MATRIX.append(transposed_dynamic_matrix)  # this is the original dynamic matrix
for matrix in DYNAMIC_MATRIX: # add 1 and log to each element of the original dynamic matrix
    for row in matrix:
        index = matrix.index(row)
        row = list(map(lambda x:math.log(x+1), row))
        matrix[index] = row

max_val_each_dimension = [] # max value of each dimension of the dynamic matrix
for dim in range(8):
    temp = []
    for num_keyword in range(len(keywords)):
        temp.append(DYNAMIC_MATRIX[num_keyword][dim])
    max_val_each_dimension.append(numpy.amax(temp))

for dim in range(8): # normalize by the max value in each dimension.
    for num_keyword in range(len(keywords)):
        if max_val_each_dimension[dim] == 0:
            DYNAMIC_MATRIX[num_keyword][dim] = [0]*len(DYNAMIC_MATRIX[num_keyword][dim])
        else:
            DYNAMIC_MATRIX[num_keyword][dim] = list(map(lambda x:x/max_val_each_dimension[dim], DYNAMIC_MATRIX[num_keyword][dim]))

"""write in txt"""
# file_name = channel+"_dynamic.txt"

jsonData = {}
for k in keywords:
    jsonData[k] = DYNAMIC_MATRIX[keywords.index(k)]

with open(file_name,'w') as f:
    json.dump(jsonData, f)
    # for num_keyword in range(len(keywords)):
    #     data = {
    #     keywords[num_keyword]:DYNAMIC_MATRIX[num_keyword]
    #     }
    #     jsonData = json.dumps(data)
    #     json.dump(jsonData,f)
    #     f.write('\n')



