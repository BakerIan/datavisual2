
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>
#include <thread>
#include <cmath>
#include <limits>
#include "rcbm_defines.h"
#include "utilities.h"
#include "rcbm.h"


int main(int argc, char *argv[])
{
    
    	if (argc != 4)
	{
		std::cout << "Correct format: RCBM Infile Outfile1 Outfile2\n";
		return -1;
	}
        
	auto start = std::chrono::high_resolution_clock::now();
	std::vector<std::thread> workers;
        initVars(std::string(argv[1]));
       	std::cout << "Th: " << Th << std::endl;
	//xCube
	vector3D<double> xCube(N, vector2D<double>(D, std::vector<double>(T, 0))); //N x D x T
	xCubeInit(xCube);

	//wCube
	vector3D<double> wCube(K, vector2D<double>(D, std::vector<double>(Tw, 0.0))); //K x D x Tw	
	vector3D<double> wCubeGrad(K, vector2D<double>(D, std::vector<double>(Tw, 0.0))); //K x D x Tw 
	vector3D<double> wCubeUpdated(K, vector2D<double>(D, std::vector<double>(Tw, 0.0))); //K x D x Tw 
	vector3D<double> wCubeDiff(K, vector2D<double>(D, std::vector<double>(Tw, 0.0))); //K x D x Tw 
	vector3D<double> outputWCube;
	wCubeInit(wCube);

	//hCube 
	vector3D<double> hCubeTokens(N, vector2D<double>(K, std::vector<double>(Th, 0.0))); //N x K x Th
	vector3D<double> outputHCubeTokens;
        
	unsigned int iterations = 0;
	double cost = std::numeric_limits<double>::infinity();
	double outputCost;

	while (1)
	{
		std::cout << "Iteration " << iterations << std::endl;
		//one_norm.m
		vector3DReset(wCubeGrad); 
		optimizeH(hCubeTokens, xCube, wCube);
		calculateWKGrad(wCubeGrad, xCube, wCube, hCubeTokens);

		vector3DReset(wCubeUpdated);
		vector3DReset(wCubeDiff);
		opt_over_w_cube(wCubeUpdated, wCube, xCube, hCubeTokens, wCubeGrad);
		for (int j = 0; j < K; ++j)
		{
			for (int k = 0; k < D; ++k)
			{
				for (int m = 0; m < Tw; ++m)
				{
					wCubeDiff[j][k][m] += wCubeUpdated[j][k][m] - wCube[j][k][m];
				}
			}
		}

		if (std::sqrt(normFrob3DSquared(wCubeDiff)) <= epsilon || iterations >= nIter)
		{
			if (iterations >= nIter)
				std::cout << "Warning: Did not converge in " << nIter << " iterations" << std::endl;
			else
				std::cout << "Good: Converged in " << iterations << " iterations" << std::endl;
			double costNew = obj_function(xCube, hCubeTokens, wCubeUpdated);
			if (costNew < cost)
			{
				outputWCube = wCubeUpdated;
				outputCost = costNew;
				outputHCubeTokens = hCubeTokens;
			}
			std::cout << "Solution Update." << std::endl;
			break;
		}
		wCube = wCubeUpdated;
		iterations++;
	}

	std::chrono::duration<double, std::ratio<1>> runtime = (std::chrono::high_resolution_clock::now() - start);
	std::cout << "Runtime: " << runtime.count() << " s" << std::endl;
	std::cout << "Cost: " << outputCost << std::endl;

        // output the sorted index (tianxi added))
        std::vector<int> index (K, 0);
        sort_h_cube(outputHCubeTokens, index);
        std::cout << "********sorted index*********** " << std::endl;
        for (int k = 0; k < K; ++k)
        {
            std::cout << index[k] << ' ';
        }
	std::cout << std::endl << "*************************************" << std::endl;
        
	//Output wCube
        vector3D<double> outputWCube_sorted(K, vector2D<double>(D, std::vector<double>(Tw, 0.0))); 
	for (int n = 0; n < K; ++n)
	{
		for (int i = 0; i < D; ++i)
		{
			for (int j = 0; j < Tw; ++j)
			{
				outputWCube_sorted[n][i][j] = outputWCube[index[n]][i][j];
			}
		}
	}

        int bin = Th/max_pooling_factor;
        vector3D<double> outputHCubeTokens_sorted(N, vector2D<double>(filter_num_lv2, std::vector<double>(bin, 0.0))); //N (Tokens) x K(dim)x bin  (Tokens,filter_num_lv2,bin)
        for (int k = 0; k < filter_num_lv2; ++k)
        {
            for (int m=0; m < bin; ++m)
            {
                for (int n = 0; n < N; ++n)
                {
                    double max_val = -100.00;
                    for (int j=0;j<(max_pooling_factor);++j)
                    {
                        int t = m*max_pooling_factor+j;
                        max_val = max_val <= outputHCubeTokens[n][index[k]][t] ? outputHCubeTokens[n][index[k]][t] : max_val;
                    }
                    outputHCubeTokens_sorted[n][k][m] = max_val;
                }
            }
        }
        
        dumpJson(outputWCube_sorted, std::string(argv[2]));
        dumpJson_H(outputHCubeTokens_sorted, std::string(argv[3]),bin);
	return 0;
}
