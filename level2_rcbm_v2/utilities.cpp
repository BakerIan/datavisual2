#include "utilities.h"
#include <algorithm> //std::fill

std::vector<int> threadAssignment(unsigned int dataNum, unsigned int threadNum)
{
	unsigned int equal = (int)floor(dataNum / threadNum);
	unsigned int leftover = dataNum % threadNum;
	unsigned int temp = 0;

	std::vector<int> index(threadNum + 1);

	for (unsigned int i = 0; i < threadNum + 1; ++i)
	{
		index[i] = temp;
		temp += equal;
		if (i < leftover)
			temp++;
	}
	return index;
}

void vector2DReset(vector2D<double> &X)
{
	size_t x = X.size();
	for (unsigned int i = 0; i < x; ++i)
	{
		std::fill(X[i].begin(), X[i].end(), 0);
	}
}

void vector3DReset(vector3D<double> &X)
{
	size_t x = X.size();
	size_t y = X[0].size();

	for (unsigned int i = 0; i < x; ++i)
	{
		for (unsigned int j = 0; j < y; ++j)
		{
			std::fill(X[i][j].begin(), X[i][j].end(), 0);
		}
	}
}
