
#ifndef __RCBM_H__
#define __RCBM_H__

#include "rcbm_defines.h"
#include <memory>
#include <fstream>

void initVars(std::string infile);
void xCubeInit(vector3D<double> & xCube);
void wCubeInit(vector3D<double> & wCube);

void optimizeH(vector3D<double> &hCubeTokens, vector3D<double> &xCube, vector3D<double> &wCube);
void opt_over_h_threaded(vector3D<double> &hCubeToken, vector3D<double> &X, vector3D<double>&W, double t_h, int start, int end);
void opt_over_h_one_token(vector2D<double> &hCubeToken, vector2D<double> &X, vector3D<double> &W, double t_h);
void calcDeltaX(vector2D<double> &deltaX, vector2D<double> &X, vector3D<double> &wCube, vector2D<double> &hOneToken);
void convolution(vector2D<double> &convolved, vector2D<double> &W, std::vector<double> &h);
void deconvolution(std::vector<double> &deconvolved, vector2D<double> &W, vector2D<double> &X);


void calculateWKGrad(vector3D<double> &wCubeGrad, vector3D<double> &xCube, vector3D<double> &wCube, vector3D<double> &hCubeTokens);
void gradientWK(vector3D<double> &wCubeGradOneToken, vector2D<double> &hCubeToken, vector2D<double> &X, vector3D<double> W);
void gradientWK_threaded(vector3D<double> &wCubeGradTemp, vector3D<double> &hCubeTokens, vector3D<double> &xCube, vector3D<double> &wCube, int start, int end);

void opt_over_w_cube(vector3D<double> &UpdatedW, vector3D<double> &W, vector3D<double> &X, vector3D<double> &hCubeTokens, vector3D<double> &wCubeGrad);

double obj_function(vector3D<double> &xCube, vector3D<double> &hCubeTokens, vector3D<double> &wCubeUpdated);
void soft_threshold(std::vector<double> &Y, std::vector<double> &X, double lambda);
double normFrob2DSquared(vector2D<double> &X);
double normFrob3DSquared(vector3D<double> &X);
double normOne2D(vector2D<double> &X);
double normOne3D(vector3D<double> &X);
//void importXCube(std::shared_ptr<std::ifstream> fileIn, vector3D<double> &xCube, int n);

// tianxi added below
void sort_h_cube(vector3D<double> &outputHCubeTokens, std::vector<int> &index);
void maxpooling(vector3D<double> &outputHCubeTokens, int factor);
void dumpJson_H(vector3D<double> &outputHCubeTokens_sorted, std::string outfile, int bin);

// Ryan
void dumpJson(vector3D<double> &wCube, std::string outfile);
#endif
