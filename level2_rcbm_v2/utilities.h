
#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include "rcbm_defines.h"

std::vector<int> threadAssignment(unsigned int dataNum, unsigned int threadNum);
void vector2DReset(vector2D<double> &X);
void vector3DReset(vector3D<double> &X);

template <typename T> inline int sgn(T val)
{
	return (T(0) < val) - (val < T(0));
}
#endif