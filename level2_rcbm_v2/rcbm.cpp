
#include "rcbm.h"
#include "utilities.h"
#include <thread>
#include <string>
#include <algorithm>
#include <iostream>
#include <random>
#include "json11.hpp"

int D;
int T;
int N;
int K;
int Tw;
int Th;
json11::Json data;

void initVars(std::string infile)
{
	std::ifstream FH_inputJson(infile, std::ios::in | std::ios::binary);
	std::string inputJson;
	FH_inputJson.seekg(0, std::ios::end);
	inputJson.resize(FH_inputJson.tellg());
	FH_inputJson.seekg(0, std::ios::beg);
	FH_inputJson.read(&inputJson[0], inputJson.size());
	FH_inputJson.close();

	std::string err;
	data = json11::Json::parse(inputJson, err);
	N = (int) data.object_items().size();
	D = (int) data.object_items().begin()->second.array_items().size();
	T = (int) data.object_items().begin()->second[0].array_items().size();
	K = 10;
	Tw = D;
	Th = T + Tw - 1;

	FH_inputJson.close();
}

void xCubeInit(vector3D<double> &xCube)
{
	int n = 0;
	for (auto it = data.object_items().begin(); it != data.object_items().end(); ++it)
	{
		for (int i = 0; i < D; ++i)
		{
			for (int j = 0; j < T; ++j)
			{
				xCube[n][i][j] = it->second[i][j].number_value();
			}
		}
		n++;
	}
}

void wCubeInit(vector3D<double> &wCube)
{
#if FIXED_W
	std::vector<std::shared_ptr<std::ifstream>> wCubeFH;
	for (int n = 1; n < (K + 1); ++n)
	{
		wCubeFH.push_back(std::make_shared<std::ifstream>("WCubeStatic/W_cube" + std::to_string(n) + ".txt"));
	}
	for (int i = 0; i < K; ++i)
	{
		for (int j = 0; j < D; ++j)
		{
			for (int k = 0; k < Tw; ++k)
			{
				*(wCubeFH[i]) >> wCube[i][j][k];
			}
		}
	}
	for (int n = 0; n < K; ++n)
	{
		wCubeFH[n]->close();
	}
#else
	//rand_init_W_cube.m
	std::mt19937 mt_gen((unsigned int ) time(NULL));
	std::uniform_real_distribution<> dist(-1, 1);

	for (int n = 0; n < K; ++n)
	{
		double mean = 0;
		double norm = 0;
		for (int i = 0; i < D; ++i)
		{
			for (int j = 0; j < Tw; ++j)
			{
				double temp = dist(mt_gen);
				wCube[n][i][j] = temp;
				mean += temp;
				norm += std::pow(temp, 2);
			}
		}
		mean /= (D*Tw);
		norm = std::sqrt((double)norm); //NOTE: CHECK TO SEE IF THIS IS OKAY
		for (int i = 0; i < D; ++i)
		{
			for (int j = 0; j < Tw; ++j)
			{
				wCube[n][i][j] = (wCube[n][i][j] - mean) / norm;
			}
		}
	}
#endif
}

void optimizeH(vector3D<double> &hCubeTokens, vector3D<double> &xCube, vector3D<double> &wCube)
{
	std::vector<std::thread> workers;
	double t_h = alpha / normOne3D(wCube);

#if (NUM_THREADS==1)
	for (unsigned int i = 0; i < N; ++i)
	{
		opt_over_h_one_token(hCubeTokens[i], xCube[i], wCube, t_h);
	}
#else
	std::vector<int>limits = threadAssignment(N, NUM_THREADS);
	for (unsigned int i = 0; i < NUM_THREADS; ++i)
	{
		workers.push_back(std::thread(opt_over_h_threaded, std::ref(hCubeTokens), std::ref(xCube), std::ref(wCube), t_h, limits[i], limits[i + 1]));
	}
	for (auto &w : workers)
		w.join();
	workers.clear();
#endif
}

void opt_over_h_threaded(vector3D<double> &hCubeToken, vector3D<double> &X, vector3D<double>&W, double t_h, int start, int end)
{
	for (int i = start; i < end; ++i)
	{
		opt_over_h_one_token(hCubeToken[i], X[i], W, t_h);
	}
}
void opt_over_h_one_token(vector2D<double> &hCubeToken, vector2D<double> &X, vector3D<double> &W, double t_h)
{
	vector2D<double> deltaX(D, std::vector<double>(T, 0));
	calcDeltaX(deltaX, X, W, hCubeToken);
	double cost = 0.5*normFrob2DSquared(deltaX) + std::pow((double)sigma, 2) / beta*normOne2D(hCubeToken);

	std::vector<double> gradHK(Th, 0);
	vector2D<double> wCubeTemp;
	std::vector<double> diff(Th, 0);
	std::vector<double> y(Th, 0);
	vector2D<double> deltaXNew(D, std::vector<double>(T, 0));
	while (1)
	{
		for (int i = 0; i < K; ++i)
		{
			std::fill(gradHK.begin(), gradHK.end(), 0);
			wCubeTemp = W[i]; //K x D x Tw
			for (int j = 0; j < D; ++j)
			{
				std::reverse(wCubeTemp[j].begin(), wCubeTemp[j].end());
			}
			deconvolution(gradHK, wCubeTemp, deltaX);
			std::fill(diff.begin(), diff.end(), 0);
			for (int j = 0; j < Th; ++j)
			{
				diff[j] = hCubeToken[i][j] - t_h*gradHK[j];
			}
			std::fill(y.begin(), y.end(), 0);
			soft_threshold(y, diff, (std::pow((double)sigma, 2) / beta*t_h));
			for (int j = 0; j < Th; ++j)
			{
				hCubeToken[i][j] = y[j] <= 0 ? 0 : y[j];
			}
		}
		vector2DReset(deltaXNew);
		calcDeltaX(deltaXNew, X, W, hCubeToken);
		double costNew = 0.5*normFrob2DSquared(deltaXNew) + std::pow((double)sigma, 2) / beta * normOne2D(hCubeToken);
		if (std::abs((cost - costNew)) < epsilon)
			break;
		deltaX = deltaXNew;
		cost = costNew;
	}
}

void calcDeltaX(vector2D<double> &deltaX, vector2D<double> &X, vector3D<double> &wCube, vector2D<double> &hOneToken)
{
	vector2D<double> convolved(D, std::vector<double>(T, 0));
	for (int i = 0; i < K; ++i)
	{
		vector2DReset(convolved);
		convolution(convolved, wCube[i], hOneToken[i]);
		for (int j = 0; j < D; ++j)
		{
			for (int k = 0; k < T; ++k)
			{
				deltaX[j][k] += convolved[j][k];
			}
		}
	}
	for (int j = 0; j < D; ++j)
	{
		for (int k = 0; k < T; ++k)
		{
			deltaX[j][k] -= X[j][k];
		}
	}
}

void convolution(vector2D<double> &convolved, vector2D<double> &W, std::vector<double> &h)
{
	size_t Tw_temp = W[0].size();
	size_t T_temp = h.size() + 1 - Tw_temp;
	for (int i = 0; i < D; ++i)
	{
		for (size_t j = 0; j < T_temp; ++j)
		{
			for (size_t k = 0; k < Tw_temp; ++k)
			{
				convolved[i][j] += (h[j + Tw_temp - k - 1] * W[i][k]);
			}
		}
	}
}

void deconvolution(std::vector<double> &deconvolved, vector2D<double> &W, vector2D<double> &X)
{
	for (int i = 0; i < (T + Tw - 1); ++i)
	{
		for (int j = 0; j < D; ++j)
		{
			for (int k = 0; k < Tw; ++k)
			{
				int temp = i - k;
				if (temp >= 0 && temp < T)
					deconvolved[i] += X[j][temp] * W[j][k];
			}
		}
	}
}

void calculateWKGrad(vector3D<double> &wCubeGrad, vector3D<double> &xCube, vector3D<double> &wCube, vector3D<double> &hCubeTokens)
{
	std::vector<std::thread> workers;
#if(NUM_THREADS==1)
	for (unsigned int i = 0; i < N; ++i)
	{
		vector3DReset(wCubeGradOneToken);
		gradientWK(wCubeGradOneToken, hCubeTokens[i], xCube[i], wCube);
		for (unsigned int j = 0; j < K; ++j)
		{
			for (int k = 0; k < D; ++k)
			{
				for (int m = 0; m < Tw; ++m)
				{
					wCubeGrad[j][k][m] += wCubeGradOneToken[j][k][m];
				}
			}
		}
	}
#else
	std::vector<int>limits = threadAssignment(N, NUM_THREADS);
	std::vector<vector3D<double>> wCubeGradTemp(NUM_THREADS, vector3D<double>(K, vector2D<double>(D, std::vector<double>(Tw, 0.0)))); //K x D x Tw 
	for (unsigned int i = 0; i < NUM_THREADS; ++i)
	{
		workers.push_back(std::thread(gradientWK_threaded, std::ref(wCubeGradTemp[i]), std::ref(hCubeTokens), std::ref(xCube), std::ref(wCube), limits[i], limits[i + 1]));
	}
	for (auto &w : workers)
		w.join();
	workers.clear();
	for (int i = 0; i < NUM_THREADS; ++i)
	{
		for (int j = 0; j < K; ++j)
		{
			for (int k = 0; k < D; ++k)
			{
				for (int m = 0; m < Tw; ++m)
				{
					wCubeGrad[j][k][m] += wCubeGradTemp[i][j][k][m];
				}
			}
		}
	}
#endif
}

void gradientWK(vector3D<double> &wCubeGradOneToken, vector2D<double> &hCubeToken, vector2D<double> &X, vector3D<double> W)
{
	vector2D<double> deltaX(D, std::vector<double>(T, 0));
	calcDeltaX(deltaX, X, W, hCubeToken);
	std::vector<double> hTilde;
	for (int i = 0; i < K; ++i)
	{
		hTilde = hCubeToken[i];
		std::reverse(hTilde.begin(), hTilde.end());
		convolution(wCubeGradOneToken[i], deltaX, hTilde);
	}
}

void gradientWK_threaded(vector3D<double> &wCubeGradTemp, vector3D<double> &hCubeTokens, vector3D<double> &xCube, vector3D<double> &wCube, int start, int end)
{
	vector3D<double> wCubeGradOneToken(K, vector2D<double>(D, std::vector<double>(Tw, 0.0))); //K x D x Tw 
	for (int i = start; i < end; ++i)
	{
		vector3DReset(wCubeGradOneToken);
		gradientWK(wCubeGradOneToken, hCubeTokens[i], xCube[i], wCube);
		for (int j = 0; j < K; ++j)
		{
			for (int k = 0; k < D; ++k)
			{
				for (int m = 0; m < Tw; ++m)
				{
					wCubeGradTemp[j][k][m] += wCubeGradOneToken[j][k][m];
				}
			}
		}
	}
}

void opt_over_w_cube(vector3D<double> &UpdatedW, vector3D<double> &W, vector3D<double> &X, vector3D<double> &hCubeTokens, vector3D<double> &wCubeGrad)
{
	double t_W = alpha / normOne3D(hCubeTokens);
	size_t x = W.size();
	size_t y = W[0].size();
	size_t z = W[0][0].size();

	for (size_t i = 0; i < x; ++i)
	{
		for (size_t j = 0; j < y; ++j)
		{
			for (size_t k = 0; k < z; ++k)
			{
				UpdatedW[i][j][k] = W[i][j][k] - t_W*wCubeGrad[i][j][k];
			}
		}
	}

	for (int i = 0; i < K; ++i)
	{
		double frobNorm = std::sqrt((double)normFrob2DSquared(UpdatedW[i]));
		if (frobNorm > 1)
		{
			for (size_t j = 0; j < y; ++j)
			{
				for (size_t k = 0; k < z; ++k)
				{
					UpdatedW[i][j][k] /= frobNorm;
				}
			}
		}
	}

}

double obj_function(vector3D<double> &xCube, vector3D<double> &hCubeTokens, vector3D<double> &wCubeUpdated)
{
	double reg = 0;
	double err = 0;

	vector2D<double> deltaX(D, std::vector<double>(T, 0));
	for (int i = 0; i < N; ++i)
	{
		vector2DReset(deltaX);
		calcDeltaX(deltaX, xCube[i], wCubeUpdated, hCubeTokens[i]);
		err += (0.5* normFrob2DSquared(deltaX));
		reg += std::pow((double)sigma, 2) / beta * normOne2D(hCubeTokens[i]);
	}
	return reg + err;
}

void soft_threshold(std::vector<double> &Y, std::vector<double> &X, double lambda)
{
	size_t size = X.size();
	for (size_t i = 0; i < size; ++i)
	{
		int sign = sgn(X[i]);
		double temp = std::abs((double)X[i]) - lambda;
		temp = (temp < 0) ? 0 : temp;
		temp *= sign;
		Y[i] = temp;
	}
}

double normOne2D(vector2D<double> &X)
{
	double norm = 0;
	for (auto it1 = X.begin(); it1 != X.end(); ++it1)
	{
		for (auto it2 = it1->begin(); it2 != it1->end(); ++it2)
		{
			norm += std::abs((double)*it2);
		}
	}
	return norm;
}

double normOne3D(vector3D<double> &X)
{
	double norm = 0;
	for (std::vector<vector2D<double>>::iterator it1 = X.begin(); it1 != X.end(); ++it1)
	{
		for (std::vector<std::vector<double>>::iterator it2 = it1->begin(); it2 != it1->end(); ++it2)
		{
			for (std::vector<double>::iterator it3 = it2->begin(); it3 != it2->end(); ++it3)
			{
				norm += std::abs((double)*it3);
			}
		}
	}
	return norm;
}

double normFrob3DSquared(vector3D<double> &X)
{
	size_t n = X.size();
	double norm = 0;
	for (size_t i = 0; i < n; ++i)
	{
		norm += normFrob2DSquared(X[i]);
	}
	return norm;
}

double normFrob2DSquared(vector2D<double> &X)
{
	size_t x = X.size();
	size_t y = X[0].size();
	double norm = 0;
	for (size_t i = 0; i < x; ++i)
	{
		for (size_t j = 0; j < y; ++j)
		{
			norm += std::pow((double)X[i][j], 2);
		}
	}
	return norm;
}



void sort_h_cube(vector3D<double> &outputHCubeTokens, std::vector<int> &index)
{
    std::vector<double> sum_h (K, 0);
    for (int k = 0; k<K; ++k)
    {
        sum_h[k] = 0;
        for (int n = 0;n<N;++n)
            for (int t = 0;t<Th;++t)
            {
                sum_h[k] = sum_h[k] + std::abs(outputHCubeTokens[n][k][t]);
            }
        std::cout << sum_h[k] << std::endl;
    }
    
    std::cout << "******" << std::endl;
    int n = {0}; 
    std::generate(index.begin(), index.end(), [&n]{ return n++; });

    std::sort(  index.begin(), 
                index.end(),
                [&sum_h](int i1, int i2) { return sum_h[i1] > sum_h[i2]; } );
}

//void maxpooling(vector3D<double> &outputHCubeTokens, int factor)
//{
//}

void dumpJson(vector3D<double> &wCube, std::string outfile)  // K D Tw
{
	std::map<std::string, std::vector<std::vector<double>>> temp;

	for (int i = 0; i < K; ++i)
	{
		std::string key = "W" + std::to_string(i);
		temp[key] = std::vector<std::vector<double>>(D, std::vector<double>(Tw));
		for (int j = 0; j < D; ++j)
		{
			for (int k = 0; k < Tw; ++k)
			{
				temp[key][j][k] = wCube[i][j][k];  // K D Tw
			}
		}
	}
	json11::Json jsonContainer(temp);
	std::string outputJson = jsonContainer.dump();

	std::ofstream FH_outputJson(outfile);
	FH_outputJson << outputJson;
	FH_outputJson.close();
}

void dumpJson_H(vector3D<double> &outputHCubeTokens_sorted, std::string outfile, int bin)
{
	std::map<std::string, std::vector<std::vector<double>>> temp;

	for (int i = 0; i < N; ++i)
	{
		std::string key = "X_cube" + std::to_string(i);
		temp[key] = std::vector<std::vector<double>>(filter_num_lv2, std::vector<double>(Th));
		for (int j = 0; j < filter_num_lv2; ++j)
		{
			for (int k = 0; k < bin; ++k)
			{
				temp[key][j][k] = outputHCubeTokens_sorted[i][j][k];
			}
		}
	}
	json11::Json jsonContainer(temp);
	std::string outputJson = jsonContainer.dump();

	std::ofstream FH_outputJson(outfile);
	FH_outputJson << outputJson;
	FH_outputJson.close();
}
