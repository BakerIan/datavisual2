
#ifndef __RCBM_DEFINES_H__
#define __RCBM_DEFINES_H__

#include <vector>
#include <cmath>

#define FIXED_W 0
#define NUM_THREADS 8

template <class T>
using vector2D = std::vector<std::vector<T>>;
template <class T>
using vector3D = std::vector<std::vector<std::vector<T>>>;

//const int D = 8;
//const int T = 1440;
//const int N = 5;
//const int K = 10; // number of filters in level 1
//const int Tw = 10;
//const int Th = T + Tw - 1;


extern int D;
extern int T;
extern int N;
extern int K;
extern int Tw;
extern int Th;

const double epsilon = 0.01;
const unsigned int nIter = 300;
const double alpha = 1.9;
const double sigma = std::sqrt(0.3);
const double beta = 1;
// tianxi added
const int filter_num_lv2 = 5;
const int max_pooling_factor = 3;


#endif